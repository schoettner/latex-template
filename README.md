# LaTeX-Template
This is my LaTeX template for student papers. The template itself is based on a template of the [TU Ilmenau](https://www.tu-ilmenau.de/it-kn/studentische-arbeiten/vorlage/). For any questions feel free to contact the author.

## Introduction
The template is designed to help latex beginners to create their papers and customize the report to your own needs. To use it more efficent, there is some setup required. This section will introduce a few basics you should know when working with the template.

### Git
Git is a great tool to versionize and backup your work. It allows you to store the work securely on a remote server so even if you lose your device, your work is still accessible. However, make sure your work is only accessible to people you want to. If you are looking for a private repository without any costs, I strongly recommend using [Gitlab](https://gitlab.com). 

Explainging Git in details would be way to complicated for this introduction. For the setup you may want to ask a software developer. They should be skilled to help you creating the repository and setting it up on your PC (e.g. *Git-Bash* when working with Windows). I only give an overview of the commands you will use most of the time.

Command | Effect | When to use
--- | --- | ---
**git pull** | Download the current version of your work to your local computer | You start working on your paper
**git push** | Upload the current commit to the remote server | You want to upload your current work
**git status** | Check what files have been removed/added/modified | Make sure you commited all files
**git commit -am 'message'** | prepare all changed files to be sent to the remote server | You made some progress and want to prepare the changes to be sent to the server. `ATTENTION` Nothing is uploaded yet! Make sure to call **git push** too!
**git add .** | Add everything you can find that is not ignored | If you find that **git status** still has some files red that you want to be sent to the server, call **git add** and check that they are 'green' now with **git status**

Again their is much more that git provides you like branches, merge requets etc. Please check the online manual for this.

### Setup the paper project
When you plan to create the paper and decide to use git there is some setup required.

1. Create a new (private) project on the git server you like (again, I recommend Gitlab)
2. Open *Git-Bash* and navigate to the folder you want to place the project in
3. Use **git clone https://gitlab.com/schoettner/latex-template.git** to download the template
4. Use **git clone https://gitlab.com/your username/your project name.git** to download the (empty) project you want to use to upload your paper
5. Copy everything `EXCEPT the .git folder and LICENSE.md` from the latex-template to your project folder.
6. You are ready to work on your paper

### TeXnicCenter
Your paper needs a few initialization steps when setting up everything the first time.

The paper is setup to have two sections for your bibliography. Print-Sources and Web-Sources. Since *Bibtex* does not support this yet, you have to change to *Biber*. I explain how you can archieve this with TeXnicCenter and MikTeX. 

1. Open the output profiles menu with Build -> Define Output Profiles 
2. Create the output profile and name it *Latex -> PDF(Biber)*
2. Change the Bibtext executeable to biber.exe (should be in the same path as bibtex.exe)
3. Go to Postprocessor -> New -> Executeable -> [path to buildNomencl.bat]. This is mandatory for the nomenclature.
4. Select the created output profile when building your project.
5. The output profile is Latex -> PDF (Biber)

You can use *F7* to compile your project. Make sure to select *build output* instead of *build current file*. If you build the document, you will receive a failure, except you selected the document.tex file.

### Project Structure
The root document is the document.tex file. This file does the whole setup and includes all other files. If you want to make any changes, they are most likey required in this file. If you want to change the content of the paper you have to add more chapters. Check the template to see how its done.

### Support
Please try to google problems or changes you want to make first. If there are any Problems you can not solve yourself, you can contact the author and ask for help.