%% ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%% Chapter: Concept - Automation App
%% ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

\section{Automation App} \label{sec:runner}
The AA is a native app that the tester installs on the device. Its main purpose is to provide an interface between developer and tester. It also monitors the device during the test process. We will take a closer look at the AA in this section.

\subsection{Inter-Process Communication} \label{sec:IPC}
To communicate with the AUT, the AA relies on the operating system internal communication mechanisms. For Android, these are intents. They are an asynchronous method to send messages between components. Generally, there are two types of intents: \emph{explicit} and \emph{implicit} intents. As their names suggest, explicit intents trigger specific components explicitly. Implicit intents trigger any active component which accepts the intent. The OS decides which component can receive the intent. If there is more than one possible component, the user is asked to select a component from a list. \cite{book:intent}

Reto Meier already describes a communication idea: ``You can also use Intents to broadcast messages across the system. Applications can register Broadcast Receivers to listen for, and react to, these Broadcast Intents. This enables you to create event-driven applications based on internal, system, or third-party application events.'' \cite[p. 166]{book:professional}. An event-driven communication is exactly what is desired for the AA and the automation framework. Section \ref{sec:data} defines the data transfer mechanism.
\begin{figure}[H]
\centering
\includegraphics[width=0.9\textwidth]{autSequence}
\caption{Communication between AA and AUT for a successful Test Process}
\label{fig:autComm}
\end{figure}
When the user triggers the test process in the AA, both apps communicate as suggested in figure \ref{fig:autComm}. The AA sends the first test case to the AUT. Here the framework parses it, if the TM is enabled. The AUT returns an error message to the AA, if the parsing failed. This indicates that the framework could not parse the test case and it was skipped. The AA can then send the next test case.

If the parsing was successful, the AUT will conduct the received test case. Independent of the success, the AUT returns a \Report with the outcome of the conduction. The AA receives the report and sends the next test case. 

After the framework completed all test cases and the AA received all corresponding reports, the test process ends. The AA then sends a final closing message to the AUT. It indicates that the AUT should terminate the TM. The AA now sends the final report for the developer.

\subsection{Data Transfer} \label{sec:data}
Intents are not only used to send messages and data between processes, but also between components, e.g. between activities of an app. Three general mechanisms exist to transfer data with intents. The following section compares them with each other and decides which is most suitable.

The simplest way to transfer data is by sending it through \emph{extras}. Extras only support primitive data types, meaning that it can send data in the form of types such as String, Boolean, Integer, or Float. It is possible to bulk multiple extras with \emph{bundles}. However, it is unnecessarily complex to transfer a test case or report with primitive data types only. Another mechanism which is more suitable for custom data types is desired.

Another approach to transfer data is with a \emph{Parcelable}. It is an interface which has to be implemented by the class the process wants to transmit. This allows sending custom data types instead of primitive data types. Listing \ref{lst:parcelable} gives a minimal implementation of the interface. If non-primitive types should be sent, they also have to implement the Parcelable interface. However, this results in a lot of implementation effort with a deep hierarchy. Collections are also not handy to implement.
\input{listing/parcelable}

The third way to send data is with a \emph{Serializable}. It also is an interface which is already well established in Java. A Serializable is an object which is representable in a byte array. It is not only the data that is serialized, but also the information about the object type and what kind of data is placed inside the object \cite{book:intent}. Even though, a Serializable is also implemented with an interface, there are no methods to implement. This makes it handier than a Parcelable.

When it comes to transferring the test cases and reports, a Parcelable or Serializable is preferred. They allow sending custom types and can both be used for larger amounts of data. A Parcelable comes with a significant time benefit over a Serializable \cite{online:parcelable,book:intent}. A Serializable, on the other hand, is much simpler to implement. Since the communication from figure \ref{fig:autComm} is not time sensitive, I decided to choose a Serializable over a Parcelable. It makes modifications much easier for prototyping.

\subsection{Monitoring}
The runtime environment is different for each user. The device runs different apps and services leading to a different use of the available resources. This can change the outcome of the test process. Too little free memory or a busy processor can lead to unexpected misbehavior. Fault causes like this are difficult to discover during tests with emulators. Therefore, it is helpful to monitor the device's resources during the test process on real hardware. This supports developers with their error analysis in case of a failing test case. It is easier to identify resource issues as cause. %Other causes may be the system language or the time zone of the device.