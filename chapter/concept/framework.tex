%% ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%% Chapter: Concept - AUT
%% ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

\section{Framework Architecture} \label{sec:framework}
If we want to test an app on a remote device, we have to automate it. Security guidelines of the OS prevent this normally. To support automation, developers embed the automation framework in the app. The automation app (AA) can now send commands to the AUT to simulate human interaction with the GUI. This way, tests run in the real runtime environment and not in a setup test environment. The goal is to verify the application as it is running on the device, not under test circumstances.

Many test frameworks apply the common \emph{XUnit pattern} \cite{book:xunit} from figure \ref{fig:phases}. Especially unit test frameworks like \emph{JUnit} \cite{online:junit} rely on this pattern. It describes a test case with multiple phases. Tests of the automation framework also apply to them. Each test case has four ordered phases. A \emph{setup}, \emph{exercise}, \emph{verify} and \emph{teardown} phase. 
\begin{figure}[H]
\centering
\begin{subfigure}[b]{.5\textwidth}
	\includegraphics[height=4.8cm]{xunit}
	\centering
	\caption{Regular XUnit Pattern \cite{book:xunit}}
	\label{fig:phases}
\end{subfigure}%
\begin{subfigure}[b]{.5\textwidth}
	\includegraphics[height=4.8cm]{xunit2}
	\centering
	\caption{Adjusted new Pattern}
	\label{fig:phasesnew}
\end{subfigure}
\caption{Compare phases of the Test Patterns}
\label{fig:pattern}
\end{figure}
Section \ref{sec:compare} points out that the process time is important. The test process has to be completed as fast as possible. Because GUI tests process a fair amount of time, we adapt the standard phase processing of the XUnit pattern. Out of the four phases, setup lasts the longest in most cases \cite{book:junit}. Therefore, the sequence of the phases does change, as seen in figure \ref{fig:phasesnew}. Instead of running the setup and teardown phase for each test case, they are only executed once for the whole test process. This saves $(2 \cdot \textrm{\#Test Cases} - 2)$ setup and teardown phases. 

Running all test cases in a sequence sets strict restrictions to their design. The end of one test is the start of its successor. This is similar to a \emph{happy path} test \cite{art:happy}. One failing test case can lead to the failure of all following test cases. Therefore, test cases should be designed as independent as possible. For example, a test can return to its start activity at the end of the conduction. To guarantee that the test process conducts successfully, a deterministic starting point is mandatory. This is achieved in the setup phase. The AUT restarts and has to enable the automation of the test case's commands. This also displays the need to modify the origin pattern as seen in figure \ref{fig:phasesnew}. The test process time would increase drastically, if the app would restart for every test case.

Tearing down the test process is simple. The AUT only terminates the automation permission. This is a critical task, to prevent controlling the AUT from other sources except the automation app. We consider this and other security risks in section \ref{sec:risk} in more detail. Terminating the AUT is optional as long as the user is notified that the test process is completed.

The following sections describe the automation library's core components. Component diagrams identify close dependencies of the components. The next chapter contains their implementation details.

\subsection{Automation Service}
Figure \ref{fig:topLevel} shows the top level architecture of the automation framework. A component's color corresponds to its package. The AUT includes a \BR, to receive test cases from the AA. Android's intent mechanism implements the IPC between the AUT and the AA. It allows sending messages and data between processes. Section \ref{sec:runner} describes the communication between the processes in more detail.
\begin{figure}[H]
\centering
\includegraphics[width=0.9\textwidth]{topLevelArchitecture}
\caption{Top-Level Architecture of the Automation Library}
\label{fig:topLevel}
\end{figure}
The user has to give his explicit permission, before we can run the test process on the device. Therefore, the user enables the \emph{test mode} (TM) in the AUT. This prevents the framework from executing tests on the device without permission. The TM is disabled by default.

The \TMP notifies the \BR if the TM is enabled or not. If it is disabled, the test case is dropped immediately. Otherwise, the \BR passes the test case to the \AS. It parses the test case with the \TCP. The \RS sends an error message to the AA if the parsing caused any issue and the test case is not conducted. Otherwise, the \AS conducts the test case with the assistance of a \TCR.

\subsection{Test Case Runner}
The \TCR runs parallel to the GUI thread of the AUT and is responsible for triggering the execution of commands. It's thread is managed by the \AS. Figure \ref{fig:testcaserunneractivity} describes the runner's top level behavior with an activity diagram. 
\begin{figure}[H]
\centering
\includegraphics[width=0.6\textwidth]{testCaseRunner_Activity}
\caption{Activity Diagram of Test Case Runner}
\label{fig:testcaserunneractivity}
\end{figure}
The \ER and \VR from figure \ref{fig:testcaserunnercomponent} are dependencies of the \TCR. They are responsible for executing all commands of the test case. The next section introduces these runners in more detail.

If an exception occurs during the conduction of the test case, it is considered failing. Without any exception occurring, the test case passes. The \RS then sends the report of the test outcome, independent of the success of the test case. The \TCR then handles the next test case.
\begin{figure}[H]
\centering
\includegraphics[width=0.9\textwidth]{testCaseRunner_Component}
\caption{Dependencies of the Test Case Runner}
\label{fig:testcaserunnercomponent}
\end{figure}

\subsection{Exercise and Verification Runner} \label{sec:exec}
A test case contains a sequence of commands. Those commands define the behavior of the test case. Either the \ER or \VR are applied to execute them, depending on the command type. As the name suggests, they are responsible for handling the exercise and verify phase from figure \ref{fig:pattern}.
\begin{figure}[H]
\centering
\includegraphics[width=0.8\textwidth]{execRunner}
\caption{Execution and Verify Runner}
\label{fig:execAndVerifyRunner}
\end{figure}
To conduct a command, the framework has to access the inner structure of the AUT. Therefore, the runners apply their loader dependencies from figure \ref{fig:execAndVerifyRunner}. The \FL provides access to an instance's field; the \ML provides access to an instance's method. The runners now have access to both fields and methods. This is the major part of automating the app with the framework.

\subsection{Commands} \label{sec:command}
Section \ref{sec:exec} mentions that the runners are responsible for conducting the commands of a test case. Figure \ref{fig:commands} shows the three different types of commands: \Action, \Wait and \Assert.

An \Action is a command to describe an interaction with the AUT. In most cases this is a GUI interaction, e.g. clicking a button. It is part of the exercise phase from figure \ref{fig:phases}. The purpose of an \Action is to simulate human interaction. Therefore, to conduct the command, the runner requires access to both fields and methods. The different loader provide this access.

The \Wait command signals the framework to wait for a certain time. It can be mandatory for the test case conduction to wait for the AUT to finish a task, e.g. login the user or loading data. However, the command can also be part of a verification. Non-functional requirements often describe a process which has to be completed in a certain amount of time. A \Wait followed by an \Assert allows verifying that the app is in the right state after the specified time. We can test time-critical, non-functional requirements which are only possible with high-level tests. 

An \Assert command represents the verification phase from figure \ref{fig:phases}. It asserts that the GUI displays an expected value e.g. a text field displays a message in the correct language. The runner has to access the corresponding field of the activity. The \ML allows accessing the displayed text. It would also be possible to read the text with the \FL. However, this is close to the Android API and can cause problems with different versions. It is preferred to access the public interface of the view element.
\begin{figure}[H]
\centering
\includegraphics[width=0.6\textwidth]{command}
\caption{Abstract View of the different commands}
\label{fig:commands}
\end{figure}
The \ER handles \Action and \Wait commands, and the \VR handles the \Assert command.